package com.zhiguokong;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/*
 ACKGROUND
Sometimes items cannot be shipped to certain zip codes, and the rules for these restrictions are stored as a series of ranges of 5 digit codes. For example if the ranges are:

[94133,94133] [94200,94299] [94600,94699]

Then the item can be shipped to zip code 94199, 94300, and 65532, but cannot be shipped to 94133, 94650, 94230, 94600, or 94299.

Any item might be restricted based on multiple sets of these ranges obtained from multiple sources.

PROBLEM
Given a collection of 5-digit ZIP code ranges (each range includes both their upper and lower bounds), provide an algorithm that produces the minimum number of ranges required to represent the same restrictions as the input.

NOTES
- The ranges above are just examples, your implementation should work for any set of arbitrary ranges
- Ranges may be provided in arbitrary order
- Ranges may or may not overlap
- Your solution will be evaluated on the correctness and the approach taken, and adherence to coding standards and best practices

EXAMPLES:
If the input = [94133,94133] [94200,94299] [94600,94699]
Then the output should be = [94133,94133] [94200,94299] [94600,94699]

If the input = [94133,94133] [94200,94299] [94226,94399] 
Then the output should be = [94133,94133] [94200,94399]

Evaluation Guidelines:
Your work will be evaluated against the following criteria:
- Successful implementation
- Efficiency of the implementation
- Design choices and overall code organization
- Code quality and best practices
*/

public class ZipRange {
	
	private static ArrayList<Range> rangeList = new ArrayList<Range>();
	
	public static void main(String[] args) {	
		
		Range a = new Range(3, 5);
		Range b = new Range(10, 13);
		Range c = new Range(8, 11);
		Range d = new Range(15, 19);
		Range e = new Range(13, 16);
		
		ZipRange.AddRange(a);
		ZipRange.AddRange(b);
		ZipRange.AddRange(c);
		ZipRange.AddRange(d);
		ZipRange.AddRange(e);
		
		showRange() ;
		
	}

	public static void AddRange(Range toAdd) {
		if(rangeList.size()==0)
			rangeList.add(toAdd);
		else {
			//Add new Range, if the range overlaps with existing range, merge them
			ListIterator<Range> iter = rangeList.listIterator();
			boolean add = true; //flag
			while( iter.hasNext()) {
				Range range = iter.next();	
				//new range within existing range, no need to add
				if(range.getLower()<=toAdd.getLower() && range.getUpper()>=toAdd.getUpper()) {
					add = false;
				}
				//new range is out of existing range, add it
				else if(range.getLower()>toAdd.getUpper() || range.getUpper()<toAdd.getLower()) {
					add = true;
				}
				//Range overlaps, remove existing, update toAdd
				else {
					if(range.getLower()<toAdd.getLower())
						toAdd.setLower(range.getLower());
					if(range.getUpper()>toAdd.getUpper())
						toAdd.setUpper(range.getUpper());
					iter.remove();
					add = true;
				}
			}//end of while
			if(add)
				rangeList.add(toAdd);
		}//end of else
		
   }
	
   public static void showRange() {
	   if(rangeList.size()>0) {
		   for(Range range :rangeList) {
			   System.out.println(range.toString());
		   }
	   }
	   
   }
   
   public static int size() {
	   return rangeList.size();
   }
   
   public static List<Range> getZipRanges() {
	   return rangeList;
   }
   
   
   public static void clear() {
	   rangeList.clear();
   }

}
